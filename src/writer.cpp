/*
 * parameter_handler.cpp
 *
 * Implementation of parameter_handler.h
 */

# include "writer.h"

//// Standard includes. /////////////////////////////////////////////////////


//// Used namespaces. ///////////////////////////////////////////////////////
//using namespace PARAMETER;

 //// New includes. /////////////////////////////////////////////////////

//// Implemented functions. /////////////////////////////////////////////////


OUTPUT::OUTPUT(std::string filename)
{
	this->file_output = std::ofstream(filename);
}

void OUTPUT::write(std::string line)
{
	if (this->file_output.is_open())
	{
		this->file_output << line << "\n"; 
	}

}

void OUTPUT::close(  )
{
	this->file_output.close();
}