/*
 * DifferentialEvolution.h
 *
 */


#ifndef WRITER_H
#define WRITER_H


//// Standard includes. /////////////////////////////////////////////////////

# include <vector>
# include <map>
# include <cassert>
# include <random>
# include <iostream>
# include <fstream>

 //// Global Vars	 ////////////////////////////////////////////////
//extern std::map<std::string, int>  func_code;

//// Definitions/namespaces. ////////////////////////////////////////////////
class OUTPUT
{
	private:
		std::ofstream file_output;

	public:
		OUTPUT(std::string filename);
		void write(std::string line);
		void close(  );
};


# endif