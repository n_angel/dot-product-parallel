/*
 * DifferentialEvolution.h
 *
 */


#ifndef VECTOR_OPERATOR_H
#define VECTOR_OPERATOR_H


//// Standard includes. /////////////////////////////////////////////////////

# include <iostream>
# include <cstdio>
# include <vector>
# include <string>
# include <random>
# include <iterator>
# include <stdexcept>
# include <cstdlib>
# include <time.h> 
# include <fstream> 
# include <map>
# include <sys/types.h>
# include <unistd.h>
# include <sys/wait.h>
# include <sys/time.h>
# include <sys/types.h>
# include <sys/ipc.h>
# include <sys/sem.h>
# include <sys/types.h>
# include <unistd.h>
 #include <sstream>

#include <sys/wait.h>  /*****  For waitpid.                   *****/
#include <setjmp.h>    /*****  For sigsetjmp and siglongjmp.  *****/
#include <signal.h>
#include <unistd.h>

# include "semaphore.h"
# include "writer.h"

 //// Global Vars	 ////////////////////////////////////////////////

//// Definitions/namespaces. ////////////////////////////////////////////////

std::vector< int > random_initialization(int vSize);
double dot_product_sequential(std::vector< int > vt1, std::vector< int > vt2, OUTPUT& wrt);
double dot_product_parallel(std::vector< int > vt1, std::vector< int > vt2, int childs, OUTPUT& wrt);

# endif