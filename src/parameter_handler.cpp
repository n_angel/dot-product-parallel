/*
 * parameter_handler.cpp
 *
 * Implementation of parameter_handler.h
 */

#include "parameter_handler.h"

//// Standard includes. /////////////////////////////////////////////////////
# include <cassert>
# include <random>
# include <iostream>

//// Used namespaces. ///////////////////////////////////////////////////////
//using namespace PARAMETER;

 //// New includes. /////////////////////////////////////////////////////

//// Implemented functions. /////////////////////////////////////////////////
std::map<std::string, std::string> PARAMETER::unpack_parameters(int argc, char *argv[])
{
	std::map<std::string, std::string> params;

	if (argc % 2 == 0)
	{
		std::cout << "Parameters error!" << std::endl;
		exit(-1);
	}

	for (int i = 1; i < argc; i+=2)
	{
		params[std::string(argv[i])] = std::string(argv[i+1]);
		//std::cout <<  << std::endl;
	}

	/*
	// show content
	for (std::map<std::string, std::string>::iterator it=params.begin(); it!=params.end(); ++it)
    	std::cout << it->first << " => " << it->second << '\n';
	*/
	
    return params;
}

int PARAMETER::decode_function_name(std::string name)
{
	return 2;
}


