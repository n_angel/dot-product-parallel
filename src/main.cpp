// standard libraries
# include <iostream>
# include <cstdio>
# include <vector>
# include <string>
# include <random>
# include <iterator>
# include <stdexcept>
# include <cstdlib>
# include <time.h> 
# include <fstream> 
# include <map>
# include <sys/types.h>
# include <unistd.h>
# include <sys/wait.h>
# include <sys/time.h>
# include <sstream>

#include <sys/wait.h>  /*****  For waitpid.                   *****/
#include <setjmp.h>    /*****  For sigsetjmp and siglongjmp.  *****/
#include <signal.h>
#include <unistd.h>

# include "parameter_handler.h"
# include "vector_operator.h"
# include "semaphore.h"
# include "writer.h"

std::default_random_engine generator;


// Main
int main(int argc, char *argv[])
{   
    if (argc < 3)
    {
        std::cout << "Error en el numero de parametros" << std::endl;
        exit(-1);
    }

    std::map<std::string, std::string> params = PARAMETER::unpack_parameters(argc, argv);
    
    std::map<std::string, std::string>::iterator it;
    int flag = 0;

    //std::string file_tasks;
    int vSize;
    it = params.find("-vSize");
    if (it != params.end())
    {
        vSize = std::stoi(it->second);
        flag ++;
    }

    int childs;
    it = params.find("-childs");
    if (it != params.end())
    {
        childs = std::stoi(it->second);
        flag ++;
    }

    std::string out_filename;
    it = params.find("-outfile");
    if (it != params.end())
    {
        out_filename = it->second;
        flag ++;
    }

    if (flag < 3)
    {
        std::cout << "Error in the number of parameters" << std::endl;
        exit(-1);
    }

    OUTPUT wrt(out_filename);
    std::ostringstream strs_seq, strs_parall;

    std::vector<int> vt1 = random_initialization(vSize);
    std::vector<int> vt2 = random_initialization(vSize);

    if (childs == 1)
    {
        double sum_seq = dot_product_sequential(vt1, vt2, wrt);
        strs_parall << sum_seq;
        wrt.write("Sequential: " + strs_parall.str() + "\n\n");
    }
    else
    {
        double sum_parall = dot_product_parallel(vt1, vt2, childs, wrt);
        strs_seq << sum_parall;
        wrt.write("Parallel: " + strs_seq.str() + "\n\n");
    }
    
    
    wrt.close();

    return 0;
}

