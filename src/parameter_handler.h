/*
 * DifferentialEvolution.h
 *
 */


#ifndef PARAMETER_HANDLER_H
#define PARAMETER_HANDLER_H


//// Standard includes. /////////////////////////////////////////////////////

# include <vector>
# include <iostream>
# include <map>

 //// Global Vars	 ////////////////////////////////////////////////
//extern std::map<std::string, int>  func_code;

//// Definitions/namespaces. ////////////////////////////////////////////////

namespace PARAMETER
{
	//void insert_keys(  );
	std::map<std::string, std::string> unpack_parameters(int argc, char *argv[]);
	int decode_function_name(std::string name);


/*
namespace DE
{

std::vector< std::vector<double> > mutation
(
  double F1,
  std::vector< std::vector<double> > population
);

std::vector< std::vector<double> > cruce
(
  int CR,
  std::vector< std::vector<double> > population,
  std::vector< std::vector<double> > v
);


}  // Differential Evolution namespace
*/
}  // Parameter Handler namespace

# endif