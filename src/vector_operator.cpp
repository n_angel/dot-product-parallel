/*
 * parameter_handler.cpp
 *
 * Implementation of parameter_handler.h
 */

# include "vector_operator.h"

//// Standard includes. /////////////////////////////////////////////////////


//// Used namespaces. ///////////////////////////////////////////////////////
//using namespace PARAMETER;

 //// New includes. /////////////////////////////////////////////////////

//// Implemented functions. /////////////////////////////////////////////////
std::vector< int > random_initialization(int vSize)
{
	std::default_random_engine generator;
	std::uniform_int_distribution<int> distribution(0,10);
	std::vector<int> vt(vSize);

	for (int i = 0; i < vSize; ++i)
	{
		vt[i] = distribution(generator);
	}

	return vt;
}

double dot_product_sequential(std::vector< int > vt1, std::vector< int > vt2, OUTPUT& wrt)
{
	struct timeval tim;
	int vSize = (int)vt1.size();
	double sum = 0;

	gettimeofday(&tim, NULL);
    double t1=tim.tv_sec+(tim.tv_usec/1000000.0);
	for(double t=0; t<(1e11/vSize); t++)
	{
		for (int i = 0; i < vSize; ++i)
		{
			sum += vt1[i] * vt2[i];
		}
	}
	gettimeofday(&tim, NULL);
    double t2=tim.tv_sec+(tim.tv_usec/1000000.0);

    std::ostringstream tm;
	tm << (t2-t1);
	wrt.write(tm.str() + " seconds elapsed");

	return sum;
}

double dot_product_parallel(std::vector< int > vt1, std::vector< int > vt2, int childs, OUTPUT& wrt)
{
	struct timeval tim;

	std::default_random_engine generator;
	generator.seed(time(NULL));
	std::uniform_int_distribution<int> distribution_key(11000,50000);

	// Threads
	pid_t my_pid, parent_pid, child_pid;

	// Semaphore
    int semnum = 4;
	std::vector<int> sids(semnum);
	std::vector<key_t> semkey(semnum);
	int key_rnd = distribution_key(generator);
	for (int it_sem = 0; it_sem < semnum; ++it_sem)
	{
		semkey[it_sem] = get_key( key_rnd + it_sem );
		sids[it_sem] = open_semaphore(semkey[it_sem]);
	}
	init_semaphore(sids[0], 1);	//	Tells the children when to start work
	init_semaphore(sids[1], childs);	//	Tells the parent when to start after all the children will finish their homework
	init_semaphore(sids[2], childs);	//	Tells to parent when all the children have been thrown, wait to take the start time
	init_semaphore(sids[3], 0);	//	Semaphore for shared memory

	// Initialize Shared memory
	int shmemnum = 1;
	std::vector<int> sidshmem(shmemnum);
	sidshmem[0] = create_shared_memory( 1 * sizeof(double), generator);
	double * ptShmem;
	ptShmem = (double *) attach_shared_memory(sidshmem[0]);
	ptShmem[0] = 0;

	// Split work
	int vSize = (int)vt1.size();
	int it_c = 0; // Child number
	int lower_boundary, upper_boundary, inc;
	lower_boundary = 0;
	upper_boundary = inc = vSize / childs;
	
	while(it_c < childs)
	{
		if((child_pid = fork()) < 0 )
        {
            perror("fork failure");
            delete_semaphore(sids);
            exit(1);
        }
        // fork() == 0 for child process 
        if(child_pid == 0)
        {
        	break;
        }

        it_c ++;
        lower_boundary = upper_boundary;
        upper_boundary += inc;
	}

	// Child process
	if(child_pid == 0)
    {
    	down_semaphore(sids[2]);	// to start parent
    	wait_semaphore(sids[0]);	// Wait to get time
    	double sum = 0;
    	for(double t=0; t<(1e11/vSize); t++)
		{
			//std::cout << t << " de " << 1e11/vSize << std::endl;
			for (int i = lower_boundary; i < upper_boundary; ++i)
			{
				sum += vt1[i] * vt2[i];
			}
		}

		wait_semaphore(sids[3]);
		up_semaphore(sids[3]);
		ptShmem[0] += sum;
		down_semaphore(sids[3]);

    	down_semaphore(sids[1]);	// Finish work
    	exit(-1);
    }
    // parent process 
    else
    {
    	wait_semaphore(sids[2]);
    	// Check starting time
    	gettimeofday(&tim, NULL);
    	double t1=tim.tv_sec+(tim.tv_usec/1000000.0);
    	down_semaphore(sids[0]);
    	wait_semaphore(sids[1]);
    	// Check ending time
    	gettimeofday(&tim, NULL);
    	double t2=tim.tv_sec+(tim.tv_usec/1000000.0);
    	std::ostringstream tm;
    	tm << (t2-t1);
    	wrt.write(tm.str() + " seconds elapsed");
    }

    double result = ptShmem[0];
	delete_semaphore(sids);
	delete_shared_memory(sidshmem);

	return result;
}